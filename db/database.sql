CREATE DATABASE books
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;

CREATE SCHEMA books
    AUTHORIZATION postgres;

CREATE TABLE books.books
(
    id integer NOT NULL,
    author character varying NOT NULL,
    title character varying NOT NULL,
    category character varying,
    description character varying,
    cover character varying,
    price integer NOT NULL,
    sold boolean,
    PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS books.books
    OWNER to postgres;

ALTER TABLE IF EXISTS books.books
    ADD COLUMN image character varying;

INSERT INTO books.books(id, author, title, category, description, cover, price, sold, image) VALUES (0, 'John Doe', 'Something', 'None', 'This is example of probable book.', 'softcover', 100, false, 'C:\Users\Настя\Desktop\JAVA\bookstore\src\main\resources\static\img.png');

CREATE TABLE books.category
(
    id integer NOT NULL,
    name character varying,
    PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS books.category
    OWNER to postgres;

CREATE TABLE books.cover
(
    id integer NOT NULL,
    cover character varying,
    PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS books.cover
    OWNER to postgres;

INSERT INTO books.category(id, name) VALUES (1, 'Художня література');
INSERT INTO books.category(id, name) VALUES (2, 'Нон-фікшн');
INSERT INTO books.category(id, name) VALUES (3, 'Психологія');
INSERT INTO books.category(id, name) VALUES (4, 'Інше');

INSERT INTO books.cover (id, cover) VALUES (1, 'Тверда');
INSERT INTO books.cover (id, cover) VALUES (2, 'М`яка');

DELETE FROM books.books WHERE id=0;

INSERT INTO books.books(id, author, title, category, description, cover, price, sold, image) VALUES (0, 'Ха-Джун Чанг', 'Как устроена экономика', 4, 'Опис', 2, 150, false, 'тут картинка');
INSERT INTO books.books(id, author, title, category, description, cover, price, sold, image) VALUES (1, 'Гэри Чепмен', 'Укрощение гнева', 3, 'Опис', 2, 100, false, 'тут картинка');
INSERT INTO books.books(id, author, title, category, description, cover, price, sold, image) VALUES (2, 'Чак Паланик', 'Бойцовский клуб', 1, 'Опис', 2, 110, false, 'тут картинка');
INSERT INTO books.books(id, author, title, category, description, cover, price, sold, image) VALUES (3, 'Норвуд Робин', 'Женщины, которые любят слишком сильно', 3, 'Опис', 2, 100, false, 'тут картинка');
INSERT INTO books.books(id, author, title, category, description, cover, price, sold, image) VALUES (4, 'Джерри Аллен Койн', 'Вера против фактов', 2, 'Опис', 1, 350, false, 'тут картинка');
INSERT INTO books.books(id, author, title, category, description, cover, price, sold, image) VALUES (5, 'Паскаль Буайе', 'Объясняя религию', 2, 'Опис', 1, 400, false, 'тут картинка');

SELECT * FROM books.books;