package ua.book.bookstore.entity;

import java.util.HashMap;
import java.util.Map;

public enum Category {
    SOMETHING(0),
    LITERATURE(1),
    NONFICTION(2),
    PSYCHOLOGY(3),
    OTHERS(4);

    private int value;
    private static Map<Integer, Category> map = new HashMap<>();

    Category(int value) {
        this.value = value;
    }

    static {
        for (Category category : Category.values()) {
            map.put(category.value, category);
        }
    }

    public static Category valueOf(int category) {
        return map.get(category);
    }

    public int getValue() {
        return value;
    }
}
