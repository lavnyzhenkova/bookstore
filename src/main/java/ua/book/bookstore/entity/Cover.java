package ua.book.bookstore.entity;

import java.util.HashMap;
import java.util.Map;

public enum Cover {
    SOMETHING(0),
    HARDCOVER(1),
    SOFTCOVER(2);

    private int value;
    private static Map<Integer, Cover> map = new HashMap<>();

    Cover(int value) {
        this.value = value;
    }

    static {
        for (Cover cover : Cover.values()) {
            map.put(cover.value, cover);
        }
    }

    public static Cover valueOf(int cover) {
        return map.get(cover);
    }

    public int getValue() {
        return value;
    }
}
