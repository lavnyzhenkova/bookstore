package ua.book.bookstore.entity;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "books")
public class Book {

    @Id
    private int id;

    @NotNull
    private String title;

    private String author;

    private Category category;

    private String description;

    private Cover cover;

    @Min(value = 0, message = "Price should be positive value.")
    private int price;

    private boolean sold;

    @Column(name = "image")
    private String pathToImage;

    public void setCategory(int category) {
        switch (category) {
            case 1 -> this.category = Category.LITERATURE;
            case 2 -> this.category = Category.NONFICTION;
            case 3 -> this.category = Category.PSYCHOLOGY;
            case 4 -> this.category = Category.OTHERS;
        }
    }

    public void setCover(int cover) {
        switch (cover) {
            case 1 -> this.cover = Cover.SOFTCOVER;
            case 2 -> this.cover = Cover.HARDCOVER;
        }
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", category=" + category +
                ", description='" + description + '\'' +
                ", cover=" + cover +
                ", price=" + price +
                '}';
    }
}
