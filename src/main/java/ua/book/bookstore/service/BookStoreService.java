package ua.book.bookstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.book.bookstore.entity.Book;
import ua.book.bookstore.entity.Category;
import ua.book.bookstore.repository.BookRepository;
import ua.book.bookstore.service.exceptions.BookNotFoundException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class BookStoreService {

    @Autowired
    private BookRepository bookRepository;
    public List<Book> getAllBooks() {
        return new ArrayList<>(bookRepository.findAll());
    }
    @Transactional
    public void addNewBook(Book book) {
        int maxId = getAllBooks().stream()
                .map(Book::getId)
                .mapToInt(value -> value)
                .max()
                .orElseThrow();

        book.setId(maxId + 1);
        book.setSold(false);
        bookRepository.save(book);
    }

    public Book getBookById(int id) {
        return bookRepository.findById(id)
                .orElseThrow(() -> new BookNotFoundException("Book with id:" + id + " is not found."));
    }
    @Transactional
    public void sellBook(int id) {
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new BookNotFoundException("Book with id: " + id + " is not found."));

        book.setSold(true);
        bookRepository.save(book);
    }

    public List<Book> getBooksByAuthor(String keyword, String author) {
        return new ArrayList<>(bookRepository.findBooksByAuthorAndKeyword(keyword.toLowerCase(), author.toLowerCase()));
    }

    public List<Book> getBooksByTitle(String keyword, String title) {
        return new ArrayList<>(bookRepository.findBooksByTitleAndKeyword(keyword.toLowerCase(), title.toLowerCase()));
    }

    public List<Book> getBooksByCategory(Category category) {
        return new ArrayList<>(bookRepository.findBooksByCategory(category));
    }

//    public Book getBookByTitle(String title) {
//        return bookRepository.findBookByTitle(title.toLowerCase());
//    }
}
