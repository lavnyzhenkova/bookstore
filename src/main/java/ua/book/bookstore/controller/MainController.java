package ua.book.bookstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.book.bookstore.entity.Book;
import ua.book.bookstore.service.BookStoreService;

import java.util.List;

@Controller
public class MainController {

    @Autowired
    private BookStoreService bookStoreService;

    @GetMapping("/books")
    public String showBooks(Model model) {
        List<Book> books = bookStoreService.getAllBooks();
        model.addAttribute("books", books);
        return "books";
    }

    @GetMapping()
    public String about() {
        return "index";
    }

    @GetMapping("/contacts")
    public String contacts() {
        return "contacts";
    }

    @RequestMapping(value = "/books/description/{id}", method = RequestMethod.GET)
    public String showDescription(Model model, @PathVariable("id") int id) {
        Book book = bookStoreService.getBookById(id);
        model.addAttribute("book", book);
        return "description";
    }

//    @RequestMapping(value = "/books/description", method = RequestMethod.GET)
//    public String showDescription(Model model) {
//        Book book = bookStoreService.getBookById(id);
//        model.addAttribute("book", book);
//        return "description";
//    }

    @PostMapping("/books/add-new-book")
    public String addNewBook(@ModelAttribute("book") Book book) {
        bookStoreService.addNewBook(book);
        return "redirect:/books";
    }
}
