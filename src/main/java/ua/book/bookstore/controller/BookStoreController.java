package ua.book.bookstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.book.bookstore.entity.Book;
import ua.book.bookstore.entity.Category;
import ua.book.bookstore.service.BookStoreService;

import java.util.List;

@Controller
@RequestMapping("/books")
public class BookStoreController {

    @Autowired
    private BookStoreService bookStoreService;

    @RequestMapping("/add")
    public String addBook(Model model) {
        model.addAttribute("book", new Book());
        return "add-new-book";
    }

    public String getBookById(@PathVariable("id") int id) {
        bookStoreService.getBookById(id);
        return "redirect:/books";
    }

    @PutMapping("/sell-book/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void sellBook(@PathVariable("id") int id) {
        bookStoreService.sellBook(id);
    }

    @GetMapping("/author/{author}")
    public List<Book> getBooksByAuthor(@RequestParam String keyword,
                                       @PathVariable @RequestParam String author) {
        return bookStoreService.getBooksByAuthor(keyword, author);
    }

//    @GetMapping("/{title}")
//    public List<Book> getBooksByTitle(@RequestParam String keyword,
//                                      @PathVariable @RequestParam String title) {
//        return bookStoreService.getBooksByTitle(keyword, title);
//    }

    @GetMapping("/category/{category}")
    public List<Book> getBooksByCategory(@PathVariable @RequestParam Category category) {
        return bookStoreService.getBooksByCategory(category);
    }

//    @GetMapping("/{title}")
//    public Book getBookByTitle(@PathVariable @RequestParam String title) {
//        return bookStoreService.getBookByTitle(title);
//    }
}
