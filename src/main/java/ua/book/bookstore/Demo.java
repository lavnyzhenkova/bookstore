package ua.book.bookstore;

import ua.book.bookstore.entity.Book;
import ua.book.bookstore.entity.Category;
import ua.book.bookstore.entity.Cover;
import ua.book.bookstore.repository.BookRepository;

public class Demo {

    public static void main(String[] args) {
        Book book = new Book();
        book.setId(1);
        book.setTitle("wsdf");
        book.setAuthor("sdcw");
        book.setSold(false);
        book.setCategory(1);
        book.setCover(2);
        book.setPathToImage("ds");
        book.setPrice(100);
        book.setDescription("Dscd");

        System.out.println(book);
    }
}
