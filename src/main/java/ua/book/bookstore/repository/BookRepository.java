package ua.book.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.book.bookstore.entity.Book;
import ua.book.bookstore.entity.Category;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {

    @Query(value = "Select * from books.books b where b.title like '%?1%' OR CAST(b.id as CHAR) like '%?1%' OR LOWER(b.author) like '%?1%'" +
            "AND b.author=?2",
            nativeQuery = true)
    List<Book> findBooksByAuthorAndKeyword(String keyword, String author);

    @Query(value = "Select * from books.books b where b.title like '%?1%' OR CAST(b.id as CHAR) like '%?1%' OR LOWER(b.author) like '%?1%'" +
            "AND b.title=?2",
            nativeQuery = true)
    List<Book> findBooksByTitleAndKeyword(String keyword, String title);

    @Query(value = "Select * from books.books b where b.category=?2", nativeQuery = true)
    List<Book> findBooksByCategory(Category category);

//    @Query(value = "Select * from books.books b where b.title=?2",
//            nativeQuery = true)
//    Book findBookByTitle(String title);
}
