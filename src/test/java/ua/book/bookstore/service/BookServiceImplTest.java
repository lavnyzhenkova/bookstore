//package ua.book.bookstore.service;
//
//import org.junit.Test;
//import org.junit.function.ThrowingRunnable;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.MockitoJUnitRunner;
//import org.modelmapper.ModelMapper;
//import ua.book.bookstore.entity.Book;
//import ua.book.bookstore.entity.Category;
//import ua.book.bookstore.entity.Cover;
//import ua.book.bookstore.repository.BookRepository;
//import ua.book.bookstore.service.exceptions.BookNotFoundException;
//import ua.book.bookstore.service.exceptions.DuplicateResourceException;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertThrows;
//import static org.mockito.Mockito.*;
//
//@RunWith(MockitoJUnitRunner.class)
//public class BookServiceImplTest {
//    private final int id = 1234;
//    private final Category category = Category.NONFICTION;
//    private final String author = "John Doe";
//    private final String title = "Something";
//    //private final boolean sold = true;
//    private final String keyword = " ";
//
//    @Mock
//    private BookRepository bookRepository;
//
//    @InjectMocks
//    private BookStoreService sut;
//
//    @Test
//    public void testAddNewBook() {
//        //Arrange
//        Book book = mock(Book.class);
//        when(bookRepository.findById(id)).thenReturn(Optional.empty());
//
//        //Act
//        sut.addNewBook(book);
//
//        //Verify
//        verify(bookRepository).save(book);
//    }
//
//    @Test
//    public void testAddNewBook_Given_IdIsPresent_Then_ThrowsDuplicateResourceException() {
//        Book book = new Book(0, title, author, Category.LITERATURE, "", Cover.SOFTCOVER, 0, false, "");
//
//        BookStoreService bookStoreService = new BookStoreService(bookRepository);
//        ThrowingRunnable runMethod = () -> bookStoreService.addNewBook(book);
//        assertThrows(DuplicateResourceException.class, runMethod);
//
////        //Arrange
////        when(book.getId()).thenReturn(id);
////        when(bookRepository.findById(id)).thenReturn(Optional.ofNullable(book));
////
//        //Act
//        sut.addNewBook(book);
//    }
//
//    @Test
//    public void testGetBookById() {
//        //Arrange
//        Book book = mock(Book.class);
//        when(bookRepository.findById(id)).thenReturn(Optional.ofNullable(book));
//
//        //Act
//        Book actualBook = sut.getBookById(id);
//
//        //Assert
//        assertEquals(book, actualBook);
//    }
//
//    @Test
//    public void testGetBookById_Given_NoBookIsFoundForId_Then_ThrowsBookNotFoundException() {
//        BookStoreService bookStoreService = new BookStoreService(bookRepository);
//        ThrowingRunnable runMethod = () -> bookStoreService.getBookById(1000000);
//        assertThrows(BookNotFoundException.class, runMethod);
//
////        //Arrange
////        when(bookRepository.findById(id)).thenReturn(Optional.empty());
////
////        //Act
////        sut.getBookById(id);
//    }
//
//    @Test
//    public void testGetAllBooks() {
//        //Arrange
//        Book book = mock(Book.class);
//        List<Book> bookList = new ArrayList<>();
//        bookList.add(book);
//
//        when(bookRepository.findAll()).thenReturn(bookList);
//
//        //Act
//        List<Book> actualBook = sut.getAllBooks();
//
//        //Assert
//        assertEquals(bookList, actualBook);
//    }
//
////    @Test
////    public void testSellBook() {
////        //Arrange
////        Book book = mock(Book.class);
////        when(bookRepository.findById(id)).thenReturn(Optional.ofNullable(book));
////        when(book.getSold()).thenReturn(sold);
////
////        //Act
////        sut.sellBook(id);
////
////        //Verify
////        verify(bookRepository).save(book);
////    }
//
////    @Test
////    public void testSellBook_Given_NoBookIsPresent_Then_ThrowsBookNotFoundException() {
////        thrown.expect(BookNotFoundException.class);
////        thrown.expectMessage("Book with id: " + id + " is not found.");
////
////        //Arrange
////        when(bookRepository.findById(id)).thenReturn(Optional.empty());
////
////        //Act
////        sut.sellBook(id);
////
////    }
//
//    @Test
//    public void testGetBooksByAuthor() {
//        //Arrange
//        Book book = mock(Book.class);
//        List<Book> books = new ArrayList<>();
//        books.add(book);
//
//        when(bookRepository.findBooksByAuthorAndKeyword(keyword.toLowerCase(), author)).thenReturn(books);
//
//        //Act
//        List<Book> actualBook = sut.getBooksByAuthor(keyword, author);
//
//        //Assert
//        assertEquals(books, actualBook);
//    }
//
//    @Test
//    public void testGetBooksByTitle() {
//        //Arrange
//        Book book = mock(Book.class);
//        List<Book> books = new ArrayList<>();
//        books.add(book);
//
//        when(bookRepository.findBooksByAuthorAndKeyword(keyword.toLowerCase(), title)).thenReturn(books);
//
//        //Act
//        List<Book> actualBook = sut.getBooksByAuthor(keyword, title);
//
//        //Assert
//        assertEquals(books, actualBook);
//    }
//
//    @Test
//    public void testGetBookByCategory() {
//        //Arrange
//        Book book = mock(Book.class);
//        List<Book> books = new ArrayList<>();
//        books.add(book);
//
//
//        when(bookRepository.findBooksByCategory(category)).thenReturn(books);
//
//
//        //Act
//        List<Book> actualBook = sut.getBooksByCategory(category);
//
//        //Assert
//        assertEquals(books, actualBook);
//
//    }
//
//}
