package ua.book.bookstore.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ua.book.bookstore.entity.Book;
import ua.book.bookstore.entity.Category;
import ua.book.bookstore.service.BookStoreService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookStoreControllerTest {
    private final int id = 2134;
    private final String title = "title";
    private final String author = "author";
    private final int price = 25;
    private final Category category = Category.LITERATURE;
    private final int totalCount = 2;
    private final int sold = 2;
    private final int addByNum = 1;
    private final String keyword = "aut";

    private MockMvc mockMvc;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private ObjectMapper objectMapper;

    @Autowired
    private BookStoreController bookStoreController;

    @MockBean
    private BookStoreService bookStoreService;

    @Before
    public void setup() {
        objectMapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(this.bookStoreController).build();
    }

//    @Test
//    public void testAddNewBook() throws Exception {
//        //Arrange
//        Book book = Book.builder()
//                .id(id).title(title).author(author)
//                .category(category).price(price)
//                .build();
//
//        doNothing().when(bookStoreService).addNewBook(book);
//
//        //Act and Assert
//        mockMvc.perform(MockMvcRequestBuilders
//                .post("/api/add-new-book")
//                .content(objectMapper.writeValueAsBytes(book))
//                .contentType(MediaType.APPLICATION_JSON)
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isCreated());
//    }

//    @Test
//    public void testGetBookById() throws Exception {
//        //Arrange
//        String url = "/api/book/" + id;
//        Book book = createBook();
//        when(bookStoreService.getBookById(id)).thenReturn(book);
//
//        //Act and Assert
//        mockMvc.perform(MockMvcRequestBuilders
//                .get(url)
//                .accept(MediaType.APPLICATION_JSON))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(id))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(title))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.author").value(author));
//    }
//
//    @Test
//    public void testGetAllBooks() throws Exception {
//        //Arrange
//        List<Book> bookDtoList = Arrays.asList(createBook());
//        when(bookStoreService.getAllBooks()).thenReturn(bookDtoList);
//
//        mockMvc.perform(MockMvcRequestBuilders
//                .get("/api/book-list")
//                .accept(MediaType.APPLICATION_JSON))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").value(id))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].author").isNotEmpty())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].author").value(author))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].category").isNotEmpty())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].totalCount").isNotEmpty());
//    }
//
//    @Test
//    public void testSellBook() throws Exception {
//        //Arrange url
//        String url = "/api/sell-book/" + id;
//        doNothing().when(bookStoreService).sellBook(id);
//
//        //Act and Assert
//        mockMvc.perform(MockMvcRequestBuilders
//                .put(url))
//                .andExpect(status().isOk());
//    }
//
//
//    @Test
//    public void testGetBookByCategory() throws Exception {
//        //Arrange
//        String url = "/api/books?"
//                + "&category=" + category;
//        List<Book> bookDtoList = Collections.singletonList(createBook());
//        when(bookStoreService.getBooksByCategory(category)).thenReturn(bookDtoList);
//
//        //Act and Assert
//        mockMvc.perform(MockMvcRequestBuilders
//                .get(url)
//                .accept(MediaType.APPLICATION_JSON))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].title").value(title))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].author").value(author))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].category").value(category.name()))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].totalCount").value(totalCount));
//    }


//    private Book createBook() {
//        return Book.builder()
//                .id(id).title(title).author(author)
//                .category(category).price(price)
//                .build();
//    }


}
